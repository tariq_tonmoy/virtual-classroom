﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationClientCommon.ViewModels
{
    public class OnlineUsersViewModel : System.Object
    {
        public string Username { get; set; }


        public static bool operator ==(OnlineUsersViewModel a, OnlineUsersViewModel b)
        {
            if ((object)a == null || (object)b == null) return false;
            else if (a.Username == b.Username) return true;
            else return false;

        }

        public static bool operator !=(OnlineUsersViewModel a, OnlineUsersViewModel b)
        {
            if ((object)a != null && (object)b != null)
            {
                if (a.Username == b.Username) return false;
                else return true;
            }
            else if ((object)a == null && (object)b == null) return false;
            else return true;

        }
    }
}
