﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationClientCommon
{
    public enum CommunicationFlag { LiveVideo, LiveAudio, LiveText, ClientConnected, ClientDisconnected, SenderConnected, SenderDisconnected, ReceiverConnected, ReceiverDisconnected }
    public enum ClientType { Sender, Receiver}
    public class CommonConstants
    {
        public const int VideoBufferSize = 20064;
        public const int AudioBufferSize = 16000;
        public const int TextBufferSize = 64;
        public const int fps = 30;

    }

}
