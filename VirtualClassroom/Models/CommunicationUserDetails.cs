﻿using CommunicationClientCommon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VirtualClassroom.Services.CommunicationServices;

namespace VirtualClassroom.Models
{
    public abstract class CommunicationUserDetails
    {
        public OnlineUsersViewModel CommunicationUserViewModel { get; set; }
        //public SenderModel SenderAsSenderModel { get; set; }
        public string ConnectionID { get; set; }

    }


    public class ReceiverModel : CommunicationUserDetails
    {
        public ReceiverModel()
        {
            ConnectionID = null;
        }
        public SenderModel SenderAsSenderModel { get; set; }
        public ReceiverHandlerService ReceiverHandlerServiceReference { get; set; }
    }


    public class SenderModel : CommunicationUserDetails
    {
        public SenderModel()
        {
            ConnectionID = null;
        }
        public SenderHandlerService SenderHandlerServiceReference { get; set; }

    }
}