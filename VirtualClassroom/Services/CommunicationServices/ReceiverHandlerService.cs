﻿using CommunicationClientCommon;
using CommunicationClientCommon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using VirtualClassroom.Models;

namespace VirtualClassroom.Services.CommunicationServices
{
    public class ReceiverHandlerService
    {

        private static List<ReceiverModel> Receivers { get; set; }


        public delegate void DataReceivedFromSenderDelegate(OnlineUsersViewModel MessageSource, CommunicationFlag commFlag, string ReceiverConnectionID, byte[] Buffer);
        public event DataReceivedFromSenderDelegate OnDataReceivedFromSender;


        public delegate void ReceiverCallbackDelegate(CommunicationFlag commFlag, byte Buffer);
        public event ReceiverCallbackDelegate OnReceiverCallback;

        public ReceiverModel ReceiverModelNative { get; set; }
        public ReceiverHandlerService()
        {
            if (Receivers == null)
            {
                Receivers = new List<ReceiverModel>();
            }
        }



        public void AddReceiver(ReceiverModel receiver)
        {
            ReceiverModelNative = receiver;
            Receivers.Add(ReceiverModelNative);
            ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnMessageReceivedFromSender += SenderHandlerServiceForReceiver_OnMessageReceivedFromSender;
            ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnStatusChanged += SenderHandlerServiceForReceiver_OnStatusChanged;
        }

        void SenderHandlerServiceForReceiver_OnStatusChanged(CommunicationFlag commFlag, string[] passedParams)
        {
            if (commFlag == CommunicationFlag.ReceiverDisconnected)
            {
                string passedConnID = passedParams[0];
                if (passedConnID == ReceiverModelNative.ConnectionID)
                {
                    Receivers.Remove(ReceiverModelNative);
                    ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnMessageReceivedFromSender -= SenderHandlerServiceForReceiver_OnMessageReceivedFromSender;
                    ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnStatusChanged -= SenderHandlerServiceForReceiver_OnStatusChanged;
                }
            }
            else if (commFlag == CommunicationFlag.SenderDisconnected)
            {
                Receivers.Remove(ReceiverModelNative);
                ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnMessageReceivedFromSender -= SenderHandlerServiceForReceiver_OnMessageReceivedFromSender;
                ReceiverModelNative.SenderAsSenderModel.SenderHandlerServiceReference.OnStatusChanged -= SenderHandlerServiceForReceiver_OnStatusChanged;

            }
        }

        void SenderHandlerServiceForReceiver_OnMessageReceivedFromSender(OnlineUsersViewModel MessageSource, CommunicationFlag commFlag, byte[] Buffer)
        {
            if (OnDataReceivedFromSender != null)
            {
                if (MessageSource.Username != ReceiverModelNative.CommunicationUserViewModel.Username)
                    OnDataReceivedFromSender(MessageSource, commFlag, ReceiverModelNative.ConnectionID, Buffer);
            }
        }

        public void SenderDisconnected()
        {

        }

        public bool IsReceiverNew(ReceiverModel receiverModel, SenderModel senderModel)
        {
            return !Receivers.Exists(x => x.CommunicationUserViewModel == receiverModel.CommunicationUserViewModel && x.SenderAsSenderModel.CommunicationUserViewModel == senderModel.CommunicationUserViewModel && (receiverModel.ConnectionID == x.ConnectionID || receiverModel.ConnectionID == null));

        }

        public static ReceiverModel GetReceiverModel(string passedConnectionID)
        {
            return Receivers.FirstOrDefault(x => x.ConnectionID == passedConnectionID && passedConnectionID != null);
        }


        public static ReceiverModel GetReceiverModel(OnlineUsersViewModel receiver, string passedConnectionID)
        {
            return Receivers.FirstOrDefault(x => (x.CommunicationUserViewModel == receiver) && (x.ConnectionID == passedConnectionID && passedConnectionID != null));
        }

    }


}