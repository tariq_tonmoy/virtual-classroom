﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommunicationClientCommon;
using Microsoft.AspNet.SignalR;
using CommunicationClientCommon.ViewModels;
using VirtualClassroom.Models;

namespace VirtualClassroom.Services.CommunicationServices
{
    public class SenderHandlerService
    {
        private static List<SenderModel> Senders { get; set; }


        public delegate void SendMessageToReceiversDelegate(OnlineUsersViewModel MessageSource, CommunicationFlag commFlag, byte[] Buffer);
        public event SendMessageToReceiversDelegate OnMessageReceivedFromSender;

        public delegate void SendStatusToReceiversDelegate(CommunicationFlag commFlag, string[] passedParams);
        public event SendStatusToReceiversDelegate OnStatusChanged;



        public SenderHandlerService()
        {
            if (Senders == null)
            {
                Senders = new List<SenderModel>();
            }
        }


        public bool SenderExists(SenderModel senderModel)
        {
            return Senders.Exists(x => x.CommunicationUserViewModel == senderModel.CommunicationUserViewModel && (x.ConnectionID == senderModel.ConnectionID || senderModel.ConnectionID == null));
        }

        public SenderModel GetSenderModel(SenderModel senderModel)
        {
            return Senders.FirstOrDefault(x => x.CommunicationUserViewModel == senderModel.CommunicationUserViewModel && (x.ConnectionID == senderModel.ConnectionID || senderModel.ConnectionID == null));
        }


        public static SenderModel GetSenderModel(OnlineUsersViewModel senderUser, string senderConnectionID)
        {
            return Senders.FirstOrDefault(x => x.CommunicationUserViewModel == senderUser && (x.ConnectionID == senderConnectionID || senderConnectionID == null));
        }

        public static SenderModel GetSenderModel(string senderConnectionID)
        {
            return Senders.FirstOrDefault((x => x.ConnectionID == senderConnectionID && x.ConnectionID != null));
        }


        public void AddNewSender(SenderModel senderModel)
        {
            senderModel.SenderHandlerServiceReference = this;
            Senders.Add(senderModel);
        }


        public void SendStatusChangeToReceiver(CommunicationFlag commFlag, string[] passedParams)
        {
            if (OnStatusChanged != null)
            {
                OnStatusChanged(commFlag, passedParams);
            }
            if (commFlag == CommunicationFlag.SenderDisconnected)
            {
                SenderModel senderModel = SenderHandlerService.GetSenderModel(passedParams[0]);
                if (senderModel != null)
                    Senders.Remove(senderModel);
            }
        }

        public void SendMessageToReceiver(OnlineUsersViewModel MessageSource, CommunicationFlag commFlag, byte[] Buffer)
        {
            if (OnMessageReceivedFromSender != null)
            {
                OnMessageReceivedFromSender(MessageSource, commFlag, Buffer);
            }
        }

    }

}