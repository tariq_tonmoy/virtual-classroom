﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VirtualClassroom.Startup))]
namespace VirtualClassroom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
