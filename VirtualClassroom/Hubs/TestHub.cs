﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualClassroom.Hubs
{

    [HubNameAttribute("TestHubForVirtualClassroom")]
    public class TestHub:Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void SendMessage(string str1, string str2)
        {
            Clients.All.received("Routing from TestHub: \"" + str1 + "\" \"" + str2 + "\"");
        }
        
    }
}