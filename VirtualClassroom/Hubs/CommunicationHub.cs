﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using CommunicationClientCommon.ViewModels;
using VirtualClassroom.Services.CommunicationServices;
using System.Threading.Tasks;
using CommunicationClientCommon;
using VirtualClassroom.Models;

namespace VirtualClassroom.Hubs
{
    public class CommunicationHub : Hub
    {
        static Dictionary<string, ClientType> ClientDictionary = new Dictionary<string, ClientType>();
        public void Hello()
        {
            Clients.All.hello();

        }

        public void RegisterAsSender(OnlineUsersViewModel user)
        {
            SenderModel senderModel = new SenderModel() { CommunicationUserViewModel = user };
            SenderHandlerService senderService = new SenderHandlerService();
            if (!senderService.SenderExists(senderModel))
            {
                senderModel.ConnectionID = Context.ConnectionId;
                senderService.AddNewSender(senderModel);
                if (!ClientDictionary.ContainsKey(Context.ConnectionId))
                    ClientDictionary.Add(Context.ConnectionId, ClientType.Sender);
            }

        }




        public override Task OnDisconnected(bool stopCalled)
        {
            string connectionID = Context.ConnectionId;

            if (ClientDictionary.ContainsKey(connectionID))
            {
                if (ClientDictionary[connectionID] == ClientType.Sender)
                {
                    SenderModel senderModel = SenderHandlerService.GetSenderModel(connectionID);
                    if (senderModel != null)
                    {
                        string username = senderModel.CommunicationUserViewModel.Username;
                        senderModel.SenderHandlerServiceReference.SendMessageToReceiver(senderModel.CommunicationUserViewModel, CommunicationFlag.SenderDisconnected, System.Text.Encoding.Unicode.GetBytes(username));
                        senderModel.SenderHandlerServiceReference.SendStatusChangeToReceiver(CommunicationFlag.SenderDisconnected, new string[1] { connectionID });
                    }
                }
                else if (ClientDictionary[connectionID] == ClientType.Receiver)
                {
                    ReceiverModel receiverModel = ReceiverHandlerService.GetReceiverModel(connectionID);
                    if (receiverModel != null)
                    {
                        SenderModel senderModel = receiverModel.SenderAsSenderModel;
                        senderModel.SenderHandlerServiceReference.SendStatusChangeToReceiver(CommunicationFlag.ReceiverDisconnected, new string[1] { connectionID });
                        senderModel.SenderHandlerServiceReference.SendMessageToReceiver(receiverModel.CommunicationUserViewModel, CommunicationFlag.ReceiverDisconnected, System.Text.Encoding.Unicode.GetBytes(receiverModel.CommunicationUserViewModel.Username));
                        receiverModel.ReceiverHandlerServiceReference.OnDataReceivedFromSender -= receiverService_OnDataReceived;
                    }
                }

                ClientDictionary.Remove(connectionID);

            }
            return base.OnDisconnected(stopCalled);
        }


        public void SendToReceivers(OnlineUsersViewModel sender, CommunicationClientCommon.CommunicationFlag commFlag, byte[] ReceivedMessage)
        {

            SenderModel senderModel = SenderHandlerService.GetSenderModel(sender, Context.ConnectionId);
            if (senderModel != null)
                senderModel.SenderHandlerServiceReference.SendMessageToReceiver(sender, commFlag, ReceivedMessage);
        }

        public void RegisterAsReceiver(OnlineUsersViewModel senderUser, OnlineUsersViewModel receiverUser)
        {
            SenderModel senderModel = new SenderModel() { CommunicationUserViewModel = senderUser };
            if (new SenderHandlerService().SenderExists(senderModel))
            {
                SenderHandlerService service = new SenderHandlerService();
                senderModel = service.GetSenderModel(senderModel);
                if (senderModel != null)
                {
                    service = senderModel.SenderHandlerServiceReference;
                    ReceiverHandlerService receiverService = new ReceiverHandlerService();
                    ReceiverModel receiverModel = new ReceiverModel()
                    {

                        SenderAsSenderModel = senderModel,
                        CommunicationUserViewModel = receiverUser,
                        ReceiverHandlerServiceReference = receiverService
                    };

                    if (receiverService.IsReceiverNew(receiverModel, senderModel))
                    {
                        receiverModel.ConnectionID = Context.ConnectionId;
                        receiverService.AddReceiver(receiverModel);
                        ClientDictionary.Add(receiverModel.ConnectionID, ClientType.Receiver);
                        receiverService.OnDataReceivedFromSender += receiverService_OnDataReceived;

                    }
                }
            }

        }

        public void CallbackFromReceiver(OnlineUsersViewModel receiver, CommunicationClientCommon.CommunicationFlag commFlag, byte[] ReceivedMessage)
        {
            string connectionID = Context.ConnectionId;
            SenderModel senderModel = ReceiverHandlerService.GetReceiverModel(receiver, connectionID).SenderAsSenderModel;
            senderModel.SenderHandlerServiceReference.SendMessageToReceiver(receiver, commFlag, ReceivedMessage);
            Clients.Client(senderModel.ConnectionID).ReceiveData(receiver, commFlag, ReceivedMessage);
        }

        void receiverService_OnDataReceived(OnlineUsersViewModel MessageSource, CommunicationFlag commFlag, string ReceiverConnectionID, byte[] Buffer)
        {
            if (commFlag == CommunicationFlag.LiveAudio)
                GlobalHost.Configuration.DefaultMessageBufferSize = CommunicationClientCommon.CommonConstants.AudioBufferSize;
            else if(commFlag==CommunicationFlag.LiveVideo)
                GlobalHost.Configuration.DefaultMessageBufferSize = CommunicationClientCommon.CommonConstants.VideoBufferSize;
            else if (commFlag == CommunicationFlag.LiveText)
                GlobalHost.Configuration.DefaultMessageBufferSize = CommunicationClientCommon.CommonConstants.TextBufferSize;
            
            Clients.Client(ReceiverConnectionID).ReceiveData(MessageSource, commFlag, Buffer);
        }


    }
}