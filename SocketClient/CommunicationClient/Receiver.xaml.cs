﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using VoiceEncoder;
using CommunicationClientWithSocket.Services;
using System.IO;
using System.Windows.Media.Imaging;


namespace CommunicationClientWithSocket
{
    public partial class Receiver : UserControl
    {
        VideoCommunication VideoCommunication;
        AudioCommunication AudioCommunication;
        PcmToWave pcm = new PcmToWave();
        Queue<byte[]> VideoBuffers = new Queue<byte[]>(), AudioBuffers = new Queue<byte[]>();
        bool AudioReceived = false, VideoReceived = false;
        

        public Receiver()
        {
            InitializeComponent();
            VideoCommunication = new VideoCommunication();
            VideoCommunication.OnReceivingSuccess += videoCommunication_OnReceivingSuccess;
            VideoCommunication.OnCommunicationError += OnCommunicationError;

            AudioCommunication = new AudioCommunication();
            AudioCommunication.OnReceivingSuccess += audioCommunication_OnReceivingSuccess;
            AudioCommunication.OnCommunicationError += OnCommunicationError;
        }

        int p = 0;
        void audioCommunication_OnReceivingSuccess(System.Net.Sockets.Socket sender, byte[] buffer)
        {
            p++;
            this.Dispatcher.BeginInvoke(() =>
            {
                
                AudioBuffers.Enqueue(buffer);
                AudioReceived = true;
                if (!VideoReceived)
                {
                    
                    return;
                }
                byte[] tempBuffer = AudioBuffers.Dequeue();
                MyButton.Content = p.ToString();
                byte[] decodedBuffer = new byte[tempBuffer.Length * 2];
                VoiceEncoder.G711Audio.ALawDecoder.ALawDecode(tempBuffer, out decodedBuffer); ;


                MemoryStream ms_PCM = new MemoryStream(decodedBuffer);
                MemoryStream ms_Wave = new MemoryStream();

                pcm.SavePcmToWav(ms_PCM, ms_Wave, 16, 8000, 1);

                WaveMediaStreamSource waveStream = new WaveMediaStreamSource(ms_Wave);

                VoiceMedia.SetSource(waveStream);
                VoiceMedia.Play();
            });
        }

        void OnCommunicationError(System.Net.Sockets.Socket sender, string str)
        {
            
        }

        void videoCommunication_OnReceivingSuccess(System.Net.Sockets.Socket sender, byte[] buffer)
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                VideoBuffers.Enqueue(buffer);
                VideoReceived = true;
                if (!AudioReceived)
                {
                    return;

                }

                byte[] tempBuffer = VideoBuffers.Dequeue();
                MemoryStream ms = new MemoryStream(tempBuffer);
                BitmapImage bi = new BitmapImage();
                bi.SetSource(ms);
                ReceivedImage.Source = bi;
                ms.Close();

            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           AudioCommunication.StartReceivingAudio();
           VideoCommunication.StartReceivingVideo();
        }
    }
}
