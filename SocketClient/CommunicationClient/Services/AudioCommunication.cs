﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using VoiceEncoder;
using System.Threading.Tasks;

namespace CommunicationClientWithSocket.Services
{
    public class AudioCommunication : CommonCommunication
    {
        Socket audioSocket;
        int bufferSize, slotCount;
        int portAddress;
        string ipAddress;
        MemoryAudioSink sink = new MemoryAudioSink();

        void InitializeConnectionStrings()
        {
            this.portAddress = 4504;
            this.ipAddress = App.IpAddressForConnection;
            this.slotCount = 5;
            bufferSize = 16000;
        }


        public void StartAudioSending()
        {
            //if (audioSocket==null || !audioSocket.Connected)
            //{
            //    InitializeConnectionStrings();
            //    this.audioSocket = this.EstablishSocketConnection(ipAddress, portAddress, bufferSize, slotCount);
            //}
            if (captureSource != null)
            {
                if (captureSource.State != CaptureState.Stopped) captureSource.Stop();
                captureSource.AudioCaptureDevice = CaptureDeviceConfiguration.GetDefaultAudioCaptureDevice();




                if ((CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess()))
                {
                    if (captureSource.AudioCaptureDevice.SupportedFormats.Count > 10)
                        captureSource.AudioCaptureDevice.DesiredFormat = captureSource.AudioCaptureDevice.SupportedFormats[10];


                    sink.CaptureSource = captureSource;
                    sink.OnBufferFulfill += sink_OnBufferFulfill;
                    captureSource.Start();
                    sink.StartSending = true;
                    
                }
            
            
            
            }
        }

        
        void sink_OnBufferFulfill(object sender, EventArgs e)
        {
            
            byte[] PCM_Buffer = (byte[])sender;
            byte[] Encoded = VoiceEncoder.G711Audio.ALawEncoder.ALawEncode(PCM_Buffer);

            AudioStarted = true;
            //CallVideoTimer();
            SendData(/*audioSocket,*/ Encoded);
            
        }

        public async void StartReceivingAudio()
        {
            if (audioSocket==null ||!audioSocket.Connected)
            {
                InitializeConnectionStrings();
                
                
                this.audioSocket = this.EstablishSocketConnection(ipAddress, portAddress, bufferSize, slotCount);
                await Task.Factory.StartNew(() => 
                {
                    while (!audioSocket.Connected) 
                    { 
                        ;
                    }
                            
                });
            }
            
            
                
            ReceiveData(audioSocket);
        }

        public void StopReceivingAudio()
        {
            sink.OnBufferFulfill -= sink_OnBufferFulfill;
            if (captureSource.State != CaptureState.Stopped) captureSource.Stop();
        }

    }
}
