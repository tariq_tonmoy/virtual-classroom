﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.IO;
using FluxJpeg;
using FluxJpeg.Core;
using FluxJpeg.Core.Encoder;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CommunicationClientWithSocket.Services
{
    public class VideoCommunication : CommonCommunication
    {
        public VideoCommunication()
        {
            this.On_Video_Timer_Tick_Required += VideoCommunication_On_Video_Timer_Tick_Required;
        }

        void VideoCommunication_On_Video_Timer_Tick_Required(object sender, EventArgs e)
        {
            timer_Tick_async(sender, e);    
        }
        
        public delegate void VideoSourceSetDelegate(VideoBrush videoBrush);
        public event VideoSourceSetDelegate OnVideoSourceSet;

        int fps, videoBufferSize, portAddress, slotCount;
        string ipAddress;
        
        Socket videoSocket;
        DispatcherTimer videoTimer=new DispatcherTimer();
        Queue<MemoryStream> memoryStreamCollection = new Queue<MemoryStream>();
        


        private void InitializeConnectionStrings()
        {
            this.portAddress = 4503;
            this.ipAddress = App.IpAddressForConnection;
            this.videoBufferSize = 20064;
            this.fps = 30;
            this.slotCount = 5;
        }
        public void StartVideoSending()
        {
            if (videoSocket==null || !videoSocket.Connected)
            {
                InitializeConnectionStrings();
                videoSocket = this.EstablishSocketConnection(ipAddress, this.portAddress, this.videoBufferSize, this.slotCount);
            }
            if (captureSource != null)
            {
                if(captureSource.State!=CaptureState.Stopped)captureSource.Stop();
                captureSource.VideoCaptureDevice = CaptureDeviceConfiguration.GetDefaultVideoCaptureDevice();
                
                
               

                if ((CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess()))
                {
                    

                    VideoBrush videoBrush = new VideoBrush();
                    videoBrush.SetSource(captureSource);
                    if (OnVideoSourceSet != null)
                        OnVideoSourceSet(videoBrush);

                }
            }

            
            videoTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000 / fps);
            videoTimer.Tick += timer_Tick_async;

            memoryStreamCollection.Clear();
            AudioStarted = false;
            captureSource.Start();
            videoTimer.Start();
            //ReceiveData(videoSocket);
            
        }


         
        async void timer_Tick_async(object sender, EventArgs e)
        {


            WriteableBitmap writeableBitmap = new WriteableBitmap(Sender.GetRectangle(), null);

            await Task.Factory.StartNew(() => 
            {
                MemoryStream memoryStream = new MemoryStream();
                EncodeJpeg(writeableBitmap, memoryStream);
                memoryStreamCollection.Enqueue(memoryStream);
                if (AudioStarted)
                {
                    SendData(videoSocket, memoryStreamCollection.Dequeue().GetBuffer());
                }


                memoryStream.Close();
            
            });
            
            
           
        }

        public static void EncodeJpeg(WriteableBitmap bmp, Stream dstStream)
        {
            // Init buffer in FluxJpeg format
            int w = bmp.PixelWidth;
            int h = bmp.PixelHeight;
            int[] p = bmp.Pixels;
            byte[][,] pixelsForJpeg = new byte[3][,]; // RGB colors
            pixelsForJpeg[0] = new byte[w, h];
            pixelsForJpeg[1] = new byte[w, h];
            pixelsForJpeg[2] = new byte[w, h];

            // Copy WriteableBitmap data into buffer for FluxJpeg
            int i = 0;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int color = p[i++];
                    pixelsForJpeg[0][x, y] = (byte)(color >> 16); // R
                    pixelsForJpeg[1][x, y] = (byte)(color >> 8);  // G
                    pixelsForJpeg[2][x, y] = (byte)(color);       // B
                }
            }
            //Encode Image as JPEG
            var jpegImage = new FluxJpeg.Core.Image(new ColorModel { colorspace = ColorSpace.RGB }, pixelsForJpeg);
            var encoder = new JpegEncoder(jpegImage, 60, dstStream);
            encoder.Encode();




        }

        public async void StartReceivingVideo()
        {
            if (videoSocket==null || !videoSocket.Connected)
            {
                
                
                InitializeConnectionStrings();
                
                videoSocket = this.EstablishSocketConnection(ipAddress, this.portAddress, this.videoBufferSize, this.slotCount);
                await Task.Factory.StartNew(() => 
                { 
                    while (!videoSocket.Connected) 
                    { 
                        ;
                    }
                    
                });
            }


            ReceiveData(videoSocket);
        }

        public void StopReceivingVideo()
        {
            videoTimer.Stop();
            videoTimer.Tick -= timer_Tick_async;
            if (captureSource.State != CaptureState.Stopped) captureSource.Stop();

        }







        


    }
}
