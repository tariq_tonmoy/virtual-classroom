﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text;


namespace CommunicationClientWithSocket.Services
{
    public class InitializeCommunication
    {
        private string RemoreIp;
        private int RemotePort;
        private string ClientIdString;
        private static int BufferSize = 128;
        byte[] buffer = new byte[BufferSize];

        public delegate void ErrorOccurredDelegate(string str);
        public event ErrorOccurredDelegate OnErrorOccurred;

        public delegate void CommunicationInitializedDelegate(string IpAddress, int PortAddress);
        public event CommunicationInitializedDelegate OnCommunicationInitialized;


   


        private void InitializeConnectionStrings()
        {
            this.RemoreIp = App.IpAddressForConnection;
            this.RemotePort = 4502;
            this.ClientIdString = "Tariq|S";

        }

        public bool ConnectToServer()
        {

            InitializeConnectionStrings();
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs args = new SocketAsyncEventArgs() { RemoteEndPoint = new IPEndPoint(IPAddress.Parse(RemoreIp), RemotePort), SocketClientAccessPolicyProtocol = SocketClientAccessPolicyProtocol.Http };
            args.SetBuffer(Encoding.UTF8.GetBytes(this.ClientIdString), 0, Encoding.UTF8.GetBytes(this.ClientIdString).Length);

            args.Completed += args_Completed_Sending;
            return socket.ConnectAsync(args);


        }

        private void args_Completed_Sending(object sender, SocketAsyncEventArgs e)
        {
            Socket socket = sender as Socket;
            if (e.SocketError == SocketError.Success)
            {
                e = new SocketAsyncEventArgs();
                e.Completed += e_Completed_Receiving;
                Array.Clear(buffer, 0, buffer.Length);
                e.SetBuffer(buffer, 0, buffer.Length);
                socket.ReceiveAsync(e);
            }
            else
            {
                if (OnErrorOccurred != null)
                {
                    OnErrorOccurred(e.SocketError.ToString());
                }

            }
        }


        private void e_Completed_Receiving(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                Socket socket = sender as Socket;
                string str = Encoding.UTF8.GetString(buffer, 0, buffer.Length).Split('\0')[0];
                socket.Shutdown(SocketShutdown.Both);
                if (OnCommunicationInitialized != null)
                {
                    try
                    {
                        OnCommunicationInitialized(str.Split('|')[0], Convert.ToInt32(str.Split('|')[1]));
                    }
                    catch (Exception ex)
                    {
                        if (OnErrorOccurred != null)
                        {
                            OnErrorOccurred(ex.Message);
                        }
                    }
                }
                

            }
            else
            {

                if (OnErrorOccurred != null)
                {
                    OnErrorOccurred(e.SocketError.ToString());
                }
                
            }
        }

        
 
    }



    
}
