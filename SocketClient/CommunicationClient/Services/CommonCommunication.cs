﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;

namespace CommunicationClientWithSocket.Services
{
    public class CommonCommunication
    {
        protected static CaptureSource captureSource = new CaptureSource();

        public delegate void CommunicationErrorDelegate(Socket sender, string str);
        public delegate void SendingSuccessDelegate(Socket sender, SocketAsyncEventArgs e);
        public delegate void ReceivingSuccessDelegate(Socket sender, byte[] buffer);

        public event CommunicationErrorDelegate OnCommunicationError;
        public event SendingSuccessDelegate OnSendingSuccess;
        public event ReceivingSuccessDelegate OnReceivingSuccess;
        


        protected static bool AudioStarted { get; set; }

        bool ReceiveContinue=true;
        byte[] receivingBuffer;
        BufferManager bufferManager;
  


        

        protected Socket EstablishSocketConnection(string IpAddress,int PortAddress, int bufferSize, int slotCount)
        {

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs s = new SocketAsyncEventArgs() { RemoteEndPoint = new IPEndPoint(IPAddress.Parse(IpAddress), PortAddress), SocketClientAccessPolicyProtocol = SocketClientAccessPolicyProtocol.Http };
            s.SetBuffer(this.GetUserSpecification(),0,this.GetUserSpecification().Length);
            socket.ConnectAsync(s);
            receivingBuffer = new byte[bufferSize];
            bufferManager = new BufferManager(bufferSize, slotCount);
            
            return socket;
        }

        private byte[] GetUserSpecification()
        {

            string sender = "user1";
            string receiver = "user2";
            string returnString="";
            if (App.SpecificClientCategory == ClientCategory.Sender)
            {
                returnString = "s|" + sender;
            }
            else if (App.SpecificClientCategory == ClientCategory.Receiver)
            {
                returnString = "r|" + sender + "|" + receiver;
            }

            return Encoding.UTF8.GetBytes(returnString);
        }

        protected void SendData(Socket socket, byte[] buffer)
        {
            
            SocketAsyncEventArgs args = new SocketAsyncEventArgs();

            if (bufferManager.SetBuffer(args, buffer))
            {
                args.Completed += args_Completed_Sending;
                socket.NoDelay = true;
                socket.SendAsync(args);
            }
            
            
        }

        protected void SendData(byte[] buffer)
        {
            if (OnReceivingSuccess != null)
            {
                OnReceivingSuccess(null, buffer);
            }
        }

        void args_Completed_Sending(object sender, SocketAsyncEventArgs e)
        {
            
            if (e.SocketError == SocketError.Success)
            {
                if (OnSendingSuccess != null)
                {
                    
                    OnSendingSuccess(sender as Socket,e);
                    
                }
                bufferManager.FreeBuffer(e);
                
            }
            else
            {
                if (OnCommunicationError != null)
                    OnCommunicationError(sender as Socket,e.SocketError.ToString());
            }

          
        }

        protected void ReceiveData(Socket socket)
        {
            this.ReceiveContinue=true;
            SocketAsyncEventArgs args = new SocketAsyncEventArgs();
            args.Completed += args_Completed_Receiving;
            args.SetBuffer(receivingBuffer, 0, receivingBuffer.Length);
            socket.ReceiveAsync(args);

        }

        void args_Completed_Receiving(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                

                
                if (OnReceivingSuccess != null)
                {
                    OnReceivingSuccess(sender as Socket, receivingBuffer);
                    

                    
                }
                if (ReceiveContinue)
                    ReceiveData(sender as Socket);
                else
                {
                    ((Socket)sender).Shutdown(SocketShutdown.Receive);
                }

            }
            else
            {
                if (OnCommunicationError != null)
                {
                    OnCommunicationError(sender as Socket, e.SocketError.ToString());
                }
            }
        }

        protected void StopReceiving()
        {
            this.ReceiveContinue = false;
        }

        protected void StopCommunication(Socket socket)
        {
            socket.Shutdown(SocketShutdown.Send);
            this.ReceiveContinue = false;
        }


        public void tempEvent(Socket sender,byte[] buffer)
        {
            if (OnReceivingSuccess != null)
            {
                OnReceivingSuccess(sender, buffer);
            }
        }


        protected delegate void Timer_Tick_Delegate(object sender, EventArgs e);
        protected event Timer_Tick_Delegate On_Video_Timer_Tick_Required;
        protected void CallVideoTimer()
        {
            if (On_Video_Timer_Tick_Required != null)
            {
                On_Video_Timer_Tick_Required(null, null);
            }

        }

    }
}
