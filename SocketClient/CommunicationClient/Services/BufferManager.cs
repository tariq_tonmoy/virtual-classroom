﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CommunicationClientWithSocket.Services
{
    public class BufferManager
    {
        byte[] bufferStack;
        int bufferSizePerSlot;
        Stack<int> offsetStack;
        int currentIndex = 0;
        public BufferManager(int bufferSize, int slotCount)
        {
            this.bufferSizePerSlot = bufferSize;
            bufferStack = new byte[bufferSize * slotCount];
            offsetStack = new Stack<int>();
        }


        public bool SetBuffer(SocketAsyncEventArgs args, byte[] buffer)
        {
            if (offsetStack.Count > 0)
            {
                int index = offsetStack.Pop();
                buffer.CopyTo(bufferStack, index);
                Array.Clear(buffer, 0, buffer.Length);
                args.SetBuffer(bufferStack, index, bufferSizePerSlot);
            
            }
            else
            {
                if (bufferStack.Length - bufferSizePerSlot < currentIndex)
                {
                    //buffer.CopyTo(bufferStack, bufferStack.Length - buffer.Length);
                    //args.SetBuffer(bufferStack, bufferStack.Length - buffer.Length, buffer.Length);
                    //Array.Clear(buffer, 0, buffer.Length);
                    //currentIndex += buffer.Length;


                    return false;
                }
                buffer.CopyTo(bufferStack,currentIndex);
                Array.Clear(buffer, 0, buffer.Length);
                args.SetBuffer(bufferStack, currentIndex, bufferSizePerSlot);
                currentIndex += bufferSizePerSlot;
            }
            return true;
        }

        public void FreeBuffer(SocketAsyncEventArgs args)
        {
            offsetStack.Push(args.Offset);
            currentIndex = args.Offset;
            args.SetBuffer(null, 0, 0);
            
        }
    }
}
