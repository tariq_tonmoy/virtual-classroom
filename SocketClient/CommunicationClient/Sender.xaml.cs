﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using CommunicationClientWithSocket.Services;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading;
using VoiceEncoder;

namespace CommunicationClientWithSocket
{
    public partial class Sender : UserControl
    {
        VideoCommunication videoCommunication;
        AudioCommunication audioCommunication;

        InitializeCommunication initialCommunication;
        private static Rectangle rect;
        private PcmToWave pcm = new PcmToWave();
        
        public Sender()
        {
           

            
            InitializeComponent();
            //initialCommunication = new InitializeCommunication();
            //initialCommunication.OnErrorOccurred += initialCommunication_OnErrorOccurred;
            //initialCommunication.OnCommunicationInitialized += initialCommunication_OnCommunicationInitialized;
            

            //videoCommunication = new VideoCommunication();
            //videoCommunication.OnVideoSourceSet += videoCommunication_OnVideoSourceSet;
            ////videoCommunication.OnReceivingSuccess += videoCommunicationReceiver_OnVideoReceived;



            audioCommunication = new AudioCommunication();
            audioCommunication.OnReceivingSuccess += audioCommunication_OnReceivingSuccess;
        
        
        }

       
        void audioCommunication_OnReceivingSuccess(Socket sender, byte[] buffer)
        {
            this.Dispatcher.BeginInvoke(() =>
            {
                byte[] decodedBuffer = new byte[buffer.Length * 2];
                VoiceEncoder.G711Audio.ALawDecoder.ALawDecode(buffer, out decodedBuffer); ;


                MemoryStream ms_PCM = new MemoryStream(decodedBuffer);
                MemoryStream ms_Wave = new MemoryStream();

                pcm.SavePcmToWav(ms_PCM, ms_Wave, 16, 8000, 1);

                WaveMediaStreamSource waveStream = new WaveMediaStreamSource(ms_Wave);

                VoiceMedia.SetSource(waveStream);
                VoiceMedia.Play();
            });
            
           

        }

       

        void videoCommunication_OnVideoSourceSet(VideoBrush videoBrush)
        {
            VideoRect.Fill=videoBrush;
            Sender.rect = VideoRect;
        }


        void initialCommunication_OnCommunicationInitialized(string IpAddress, int PortAddress)
        {
           
        }

        void initialCommunication_OnErrorOccurred(string str)
        {
            Debug.WriteLine("Error : " + str);
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //if (initialCommunication.ConnectToServer())
            //{
              //  videoCommunication.StartVideoSending();
                audioCommunication.StartAudioSending();
            //}
            
        }

        void videoCommunicationReceiver_OnVideoReceived(Socket socket, byte[] buffer)
        {
            this.Dispatcher.BeginInvoke(() => 
            {
                MemoryStream ms = new MemoryStream(buffer);
                BitmapImage bi = new BitmapImage();
                bi.SetSource(ms);
                ReceivedImage.Source = bi;
                ms.Close();

            });
        
           
        }



        public static Rectangle GetRectangle()
        {
           
            return rect;
        }

       
       
    }
}
