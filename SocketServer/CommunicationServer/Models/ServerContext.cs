﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CommunicationServer.Models
{
    public class ServerContext : DbContext
    {

        public ServerContext()
            : base(@"Data Source=TARIQ-PC;Initial Catalog=VirtualClassroomDB;Integrated Security=True")
        {

        }
        public DbSet<OnlineUser> OnlineUsers { get; set; }

    }

    
}
