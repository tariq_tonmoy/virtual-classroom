﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace CommunicationServer.Models
{
    public class OnlineUser
    {
        private string username, userPort, userType;
      
        private int id;

        [Key]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

  
        
        public string Username
        {
            get { return username; }
            set { username = value; }
        }


        
        public string UserPort
        {
            get { return userPort; }
            set { userPort = value; }
        }

        public string UserType
        {
            get { return userType; }
            set { userType = value; }
        }

        public string ListeningTo { get; set; }

       
    }
}
