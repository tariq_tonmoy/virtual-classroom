namespace CommunicationServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OnlineUsers",
                c => new
                    {
                        Username = c.String(nullable: false, maxLength: 128),
                        UserIp = c.String(),
                        UserPort = c.String(),
                        UserType = c.String(),
                    })
                .PrimaryKey(t => t.Username);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OnlineUsers");
        }
    }
}
