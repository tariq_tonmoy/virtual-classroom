namespace CommunicationServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OnlineUsers", "UserLevel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OnlineUsers", "UserLevel");
        }
    }
}
