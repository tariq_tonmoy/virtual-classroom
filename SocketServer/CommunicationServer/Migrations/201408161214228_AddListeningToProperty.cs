namespace CommunicationServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddListeningToProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OnlineUsers", "ListeningTo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OnlineUsers", "ListeningTo");
        }
    }
}
