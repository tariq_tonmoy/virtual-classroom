namespace CommunicationServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReconstructDb : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.OnlineUsers");
            AddColumn("dbo.OnlineUsers", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.OnlineUsers", "Username", c => c.String());
            AddPrimaryKey("dbo.OnlineUsers", "Id");
            DropColumn("dbo.OnlineUsers", "UserLevel");
            DropColumn("dbo.OnlineUsers", "UserIp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OnlineUsers", "UserIp", c => c.String());
            AddColumn("dbo.OnlineUsers", "UserLevel", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.OnlineUsers");
            AlterColumn("dbo.OnlineUsers", "Username", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.OnlineUsers", "Id");
            AddPrimaryKey("dbo.OnlineUsers", "Username");
        }
    }
}
