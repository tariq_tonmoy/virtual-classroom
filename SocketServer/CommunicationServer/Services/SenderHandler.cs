﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServer.Services
{
    public class SenderHandler
    {
        public TcpClient Tcp_Client { get; set; }
       // public string Username { get; set; }
        public bool  IsInitiated { get; set; }
        public ClientDataType SenderDataType { get; set; }

        public delegate void ReceivedMessageDelegate(byte[] ReceivedBytes);
        public event ReceivedMessageDelegate OnMessageReceived;

        public delegate void SenderDisconnectedDelegate();
        public event SenderDisconnectedDelegate OnSenderDisconnected; 

        public  void sendData(byte[] ReceivedBytes)
        {
            if (OnMessageReceived != null)
            {
                
                OnMessageReceived(ReceivedBytes); 
            }
        }

        public void SenderDisconnected()
        {
            if (OnSenderDisconnected != null)
            {
                OnSenderDisconnected();
            }
        }
    }
}
