﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using CommunicationServer.Models;

namespace CommunicationServer.Services
{
    public enum ClientDataType { AudioData, VideoData, ChatData, OtherData, ChatAndOtherData };
    public class CommunicationInitializer
    {
        public void InitializeCommunication()
        {
            InitializeTcpConnection();
            Console.ReadLine();
        }


        private TcpListener Listener;
        private const int InitialPort = 4502;
        private static ManualResetEvent TcpClientConnected = new ManualResetEvent(false);
        private byte[] ReceivedByte;
        private int Size = 128;
        List<CommunicationMiddleware> CommunicationMiddlewareList = new List<CommunicationMiddleware>();
        int CurrentPort;
        int LowerPort;
        int UpperPort;
        string IpAddressToCommunicate = "10.220.52.220";

        
        private async void InitializeTcpConnection()
        {
            LowerPort = 4503;
            UpperPort = 4530;
            CurrentPort = LowerPort;
            

            Listener = new TcpListener(IPAddress.Any, InitialPort);
            Listener.Start();
            await Task.Run(() =>
            {
                while (true)
                {
                    

                    TcpClientConnected.Reset();
                    Listener.BeginAcceptTcpClient(OnBeginAccept, Listener);
                    TcpClientConnected.WaitOne();
                }
            });
            
            

        }

       

        private void OnBeginAccept(IAsyncResult ar)
        {
            TcpClient client = ((TcpListener)ar.AsyncState).EndAcceptTcpClient(ar);
            TcpClientConnected.Set();
            ReplyClient(client);
        
        }

        private async void ReplyClient(TcpClient PassedClient)
        {
            ReceivedByte=new byte[Size];
            

            await Task.Run(() => 
            {
                PassedClient.Client.Receive(ReceivedByte);
                string PassedString = Encoding.UTF8.GetString(ReceivedByte);
                
                

                using (var db = new ServerContext())
                {

                }

                


                string SendString = IpAddressToCommunicate+"|4503";

                SocketAsyncEventArgs s = new SocketAsyncEventArgs();
                byte[] SendByte = new byte[Size];
                SendByte = Encoding.UTF8.GetBytes(SendString);
                s.SetBuffer(SendByte, 0, SendByte.Length);
                s.Completed += (sender, args) =>
                {
                    if (s.SocketError == SocketError.Success)
                    {
                        (sender as Socket).Shutdown(SocketShutdown.Send);
                    }

                };
                PassedClient.Client.SendAsync(s);


                //CommunicationMiddleware cm = new CommunicationMiddleware(this.IpAddressToCommunicate, 4503, 20064,ClientDataType.VideoData);
                //cm.StatMiddleware();
                //CommunicationMiddleware cm2;
                //cm = new CommunicationMiddleware(this.IpAddressToCommunicate, 4504, 16000, ClientDataType.AudioData);
                //cm.StatMiddleware();
                //return;

                int tempPort = 0;
                
                if (CommunicationMiddlewareList.LastOrDefault() == null || CommunicationMiddlewareList.LastOrDefault().Port_Address != this.UpperPort + 2)
                {
                    tempPort = CommunicationMiddlewareList.LastOrDefault() == null ? this.LowerPort : CommunicationMiddlewareList.LastOrDefault().Port_Address != this.UpperPort + 2 ? this.CurrentPort : 0;


                    CommunicationMiddleware VideoCM = new CommunicationMiddleware(IpAddressToCommunicate, tempPort, 20064,ClientDataType.VideoData);
                    VideoCM.OnSenderListEmpty += tempCM_OnSenderListEmpty;
                    CommunicationMiddlewareList.Add(VideoCM);
                    
                    CommunicationMiddleware AudioCM = new CommunicationMiddleware(IpAddressToCommunicate, tempPort + 1, 16000, ClientDataType.AudioData);
                    AudioCM.OnSenderListEmpty += tempCM_OnSenderListEmpty;
                    CommunicationMiddlewareList.Add(AudioCM);


                    CommunicationMiddleware ChatAndOtherCM = new CommunicationMiddleware(IpAddressToCommunicate, tempPort + 2, 64);
                    ChatAndOtherCM.OnSenderListEmpty += tempCM_OnSenderListEmpty;
                    CommunicationMiddlewareList.Add(ChatAndOtherCM);
                    

                    VideoCM.StatMiddleware();
                    AudioCM.StatMiddleware();
                    ChatAndOtherCM.StatMiddleware();



                }

                else
                {
                    tempPort = CurrentPort;
                    CommunicationMiddleware tempCM = CommunicationMiddlewareList.FirstOrDefault(x => x.Port_Address == tempPort);
                    if (tempCM != null && !tempCM.Tcp_Listener.Server.Connected)
                        tempCM.StatMiddleware();

                    tempCM = CommunicationMiddlewareList.FirstOrDefault(x => x.Port_Address == tempPort + 1);
                    if (tempCM != null && !tempCM.Tcp_Listener.Server.Connected)
                        tempCM.StatMiddleware();

                    tempCM = CommunicationMiddlewareList.FirstOrDefault(x => x.Port_Address == tempPort + 2);
                    if (tempCM != null && !tempCM.Tcp_Listener.Server.Connected)
                        tempCM.StatMiddleware();

                }

                CurrentPort += 3;

                



               
                

               
            
            });           
        }

        void tempCM_OnSenderListEmpty(CommunicationMiddleware communicationMiddleware)
        {
            communicationMiddleware.StopMiddleware();
        }


       

    }
}
