﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationServer.Services
{
    public class ReceiverHandler
    {
        public TcpClient Tcp_Client { get; set; }
        public string Username  { get; set; }

        public SenderHandler AssignedSender { get; set; }

        public delegate void ReceiverDisconectedDelegate(ReceiverHandler PassedReceiverHandler);
        public event ReceiverDisconectedDelegate OnReceiverDisconnected;
        public void ReceiverDisconnected()
        {
            if (OnReceiverDisconnected != null)
            {
                OnReceiverDisconnected(this);
            }
        }
        public  void SendDataToReceiver(byte[] ReceivedBytes)
        {
            SocketAsyncEventArgs s = new SocketAsyncEventArgs();

            s.RemoteEndPoint = Tcp_Client.Client.LocalEndPoint;
            s.SetBuffer(ReceivedBytes, 0, ReceivedBytes.Length);


            s.Completed += (sender, args) =>
            {
                if (s.SocketError == SocketError.Success)
                {

                }
                else
                {
                    SenderDisconnected();
                    return;
                }

            };
           



            if (!Tcp_Client.Client.SendAsync(s))
            {
                SenderDisconnected();
            }


            
        }

        public void SenderDisconnected()
        {
            if (this.OnReceiverDisconnected != null)
            {
                OnReceiverDisconnected(this);
            }
            AssignedSender.OnMessageReceived -= this.SendDataToReceiver;
            AssignedSender.OnSenderDisconnected -= this.SenderDisconnected;
        }

        

    }
}
