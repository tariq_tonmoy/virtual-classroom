﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace CommunicationServer.Services
{
    public class CommunicationMiddleware
    {
        public TcpListener Tcp_Listener { get; set; }
        static ManualResetEvent TcpClientConnected = new ManualResetEvent(false);
        byte[] receivedBuffer;
        string Ip_Address;
        public int Port_Address { get; set; }
        ClientDataType MiddlewareDataType;

        

        public delegate void SenderListEmptyDelegate(CommunicationMiddleware communicationMiddleware);
        public event SenderListEmptyDelegate OnSenderListEmpty;

        



        class ClientDataStructure
        {
            public string  Username { get; set; }
            public List<SenderHandler> SenderHandlers { get; set; }
            public bool AudioReceived { get; set; }
            public bool  VideoReceived { get; set; }
            public Queue<byte[]> AudioBytes { get; set; }
            public Queue<byte[]> VideoBytes { get; set; }

            public HashSet<string> ReceiverUsernames { get; set; }

            public void RemoveReceiver(ReceiverHandler PassedReceiver)
            {
                ReceiverUsernames.Remove(PassedReceiver.Username);
                PassedReceiver.OnReceiverDisconnected -= this.RemoveReceiver;
            }
        
        }

        static Dictionary<string, ClientDataStructure> ClientDataDictionary = new Dictionary<string, ClientDataStructure>(); 


        public CommunicationMiddleware(string ipAddress, int portAddress, int bufferSize, ClientDataType PassedDataType=ClientDataType.ChatAndOtherData)
        {
            Tcp_Listener = new TcpListener(IPAddress.Parse(ipAddress), portAddress);
            receivedBuffer = new byte[bufferSize];
            this.Ip_Address = ipAddress;
            this.Port_Address = portAddress;
            MiddlewareDataType = PassedDataType;
           
        }

        public async void StatMiddleware()
        {
            Tcp_Listener.Start();
           

            
            await Task.Run(() =>
            {
                while (true)
                {
                    TcpClientConnected.Reset();
                    Tcp_Listener.BeginAcceptTcpClient(OnBeginAccept, Tcp_Listener);
                    TcpClientConnected.WaitOne();
                }
            });
        }

        public void StopMiddleware()
        {
           
            
            Tcp_Listener.Stop();
           
        }
        private void OnBeginAccept(IAsyncResult ar)
        {

            try
            {
                TcpClient client = ((TcpListener)ar.AsyncState).EndAcceptTcpClient(ar);
                TcpClientConnected.Set();
                ReceiveClient(client);
            }
            catch (Exception)
            {
                
            }

        }
        private async void ReceiveClient(TcpClient PassedClient)
        {
            await Task.Run(() =>
            {
                
                Array.Clear(receivedBuffer, 0, receivedBuffer.Length);
                PassedClient.Client.Receive(receivedBuffer);
                string SentString = Encoding.UTF8.GetString(receivedBuffer).Split('\0')[0];
                if (SentString[0] == 's')
                {
                    SenderHandler senderHandler = new SenderHandler() { Tcp_Client=PassedClient, IsInitiated=true,  SenderDataType=MiddlewareDataType};
                    string username = SentString.Split('|')[1];
                    if (!ClientDataDictionary.ContainsKey(username))
                    {
                        ClientDataDictionary.Add(username, new ClientDataStructure() { AudioBytes = new Queue<byte[]>(), VideoBytes = new Queue<byte[]>(), AudioReceived = false, VideoReceived = false, SenderHandlers = new List<SenderHandler>(), Username = username, ReceiverUsernames=new HashSet<string>() });
                    }
                    ClientDataDictionary[username].SenderHandlers.Add(senderHandler);

                    ReceiveClient(senderHandler,username);

                   

                }
                else if(SentString[0]=='r')
                {
                    ReceiverHandler receiverHandler = new ReceiverHandler();
                    receiverHandler.Username = SentString.Split('|')[2];
                    string SenderUsername = SentString.Split('|')[1];
                    receiverHandler.Tcp_Client = PassedClient;
                    receiverHandler.Tcp_Client.NoDelay = true;

                    if (ClientDataDictionary.ContainsKey(SenderUsername))
                    {
                        SenderHandler senderhandler = ClientDataDictionary[SenderUsername].SenderHandlers.FirstOrDefault(x => x.SenderDataType == this.MiddlewareDataType);
                        
                        if (senderhandler != null)
                        {
                            receiverHandler.AssignedSender = senderhandler;
                            ClientDataDictionary[SenderUsername].ReceiverUsernames.Add(receiverHandler.Username);
                            senderhandler.OnMessageReceived += receiverHandler.SendDataToReceiver;
                            senderhandler.OnSenderDisconnected+=receiverHandler.SenderDisconnected;
                            receiverHandler.OnReceiverDisconnected += ClientDataDictionary[SenderUsername].RemoveReceiver;
                        }
                    }
                    else
                    {

                    }
                    
                }
              

               


            });
        }


        private async void ReceiveClient(SenderHandler PassedSender,string PassedUsername)
        {
            TcpClient PassedClient = PassedSender.Tcp_Client;

            await Task.Run(() =>
            {

                Array.Clear(receivedBuffer, 0, receivedBuffer.Length);
                try
                {
                    PassedClient.Client.Receive(receivedBuffer);


                    if (MiddlewareDataType == ClientDataType.AudioData)
                    {
                        if (ClientDataDictionary.ContainsKey(PassedUsername))
                        {
                            ClientDataDictionary[PassedUsername].AudioBytes.Enqueue(receivedBuffer);
                            ClientDataDictionary[PassedUsername].AudioReceived = true;
                            if(ClientDataDictionary[PassedUsername].VideoReceived)
                            {
                                PassedSender.sendData(ClientDataDictionary[PassedUsername].AudioBytes.Dequeue());
                            }
                            

                        }
                    }
                    else if (MiddlewareDataType == ClientDataType.VideoData)
                    {
                        if (ClientDataDictionary.ContainsKey(PassedUsername))
                        {
                            ClientDataDictionary[PassedUsername].VideoBytes.Enqueue(receivedBuffer);
                            ClientDataDictionary[PassedUsername].VideoReceived = true;
                            if (ClientDataDictionary[PassedUsername].AudioReceived)
                            {
                                PassedSender.sendData(ClientDataDictionary[PassedUsername].VideoBytes.Dequeue());
                            }


                        }

                    }
                    else
                    {
                        PassedSender.sendData(receivedBuffer);
                    }
                   
                    
                    
                    
                    
                    
                    ReceiveClient(PassedSender,PassedUsername);
                }
                catch (Exception)
                {


                    PassedSender.SenderDisconnected();

                    if(ClientDataDictionary.ContainsKey(PassedUsername))
                    {
                       
                        ClientDataDictionary.Remove(PassedUsername);
                        if (ClientDataDictionary.Count == 0 && OnSenderListEmpty != null)
                            OnSenderListEmpty(this);
                    }

                }




            });
           
        }
    }
}
