﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CommunicationServer.Models;
using CommunicationServer.Services;

namespace CommunicationServer
{
    class Program
    {
       // public static string IpAddressToCommunicate = "192.168.0.100";
        static void Main(string[] args)
        {
            using (var v = new ServerContext())
            {
                OnlineUser u = new OnlineUser() { ListeningTo = "a", Username = "b", UserPort = "123", UserType = "r" };
                v.OnlineUsers.Add(u);
                v.SaveChanges();
            }

            CommunicationInitializer com = new CommunicationInitializer();
            com.InitializeCommunication();
        }
    }
}
