﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.IO;
using FluxJpeg;
using FluxJpeg.Core;
using FluxJpeg.Core.Encoder;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommunicationClientCommon;
using System.Diagnostics;

namespace CommunicationClientSL.Services
{
    public class VideoCommunicationService : CommonCommunicationService
    {
        public delegate void VideoSourceSetDelegate(VideoBrush videoBrush);
        public event VideoSourceSetDelegate OnVideoSourceSet;
        //bool audioBufferFullfilled = false;


        DispatcherTimer videoTimer = new DispatcherTimer();

        public VideoCommunicationService()
        {
            //On_Video_Timer_Tick_Required += VideoCommunicationService_On_Video_Timer_Tick_Required;
        }

        //void VideoCommunicationService_On_Video_Timer_Tick_Required(object sender, EventArgs e, VideoCommunicationService VideoService)
        //{
        //    audioBufferFullfilled = true;
        //    VideoService.timer_Tick(sender, e);

        //}

        public void StartSendingVideo()
        {
            VideoBrush videoBrush = new VideoBrush();
            videoBrush.SetSource(captureSource);
            if (OnVideoSourceSet != null)
            {
                OnVideoSourceSet(videoBrush);
                videoTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000 / CommonConstants.fps);
                videoTimer.Tick += timer_Tick_async;
                videoTimer.Start();
            }

            //if (captureSource != null)
            //{
               
            //    if (captureSource.State != CaptureState.Stopped) captureSource.Stop();
            //    captureSource.VideoCaptureDevice = CaptureDeviceConfiguration.GetDefaultVideoCaptureDevice();




            //    if ((CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess()))
            //    {


                    
            //    }
            //}



        }

        Queue<MemoryStream> StreamQueue = new Queue<MemoryStream>();
        private  void timer_Tick_async(object sender, EventArgs e)
        {
            try
            {
                if (StreamQueue.Count > 50) StreamQueue.Dequeue();
                WriteableBitmap writeableBitmap = new WriteableBitmap(Sender.GetVideoRect(), null);
                MemoryStream memoryStream = new MemoryStream();
                EncodeJpeg(writeableBitmap, memoryStream);
                StreamQueue.Enqueue(memoryStream);
                
                //await Task.Factory.StartNew(() =>
                //{
                //    if (true || AudioStarted)
                //    {
                        
                        SendData(CommunicationFlag.LiveVideo, StreamQueue.Dequeue().GetBuffer());
                        //SendData(CommunicationFlag.LiveVideo, memoryStream.GetBuffer());
                   // }



                //});
                memoryStream.Close();
            }
            catch (Exception ex)
            {
                
            }

        }


        public static void EncodeJpeg(WriteableBitmap bmp, Stream dstStream)
        {
            // Init buffer in FluxJpeg format
            int w = bmp.PixelWidth;
            int h = bmp.PixelHeight;
            int[] p = bmp.Pixels;
            byte[][,] pixelsForJpeg = new byte[3][,]; // RGB colors
            pixelsForJpeg[0] = new byte[w, h];
            pixelsForJpeg[1] = new byte[w, h];
            pixelsForJpeg[2] = new byte[w, h];

            // Copy WriteableBitmap data into buffer for FluxJpeg
            int i = 0;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int color = p[i++];
                    pixelsForJpeg[0][x, y] = (byte)(color >> 16); // R
                    pixelsForJpeg[1][x, y] = (byte)(color >> 8);  // G
                    pixelsForJpeg[2][x, y] = (byte)(color);       // B
                }
            }
            //Encode Image as JPEG
            var jpegImage = new FluxJpeg.Core.Image(new ColorModel { colorspace = ColorSpace.RGB }, pixelsForJpeg);
            var encoder = new JpegEncoder(jpegImage, 60, dstStream);
            encoder.Encode();

        }
    }
}
