﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using VoiceEncoder;
using System.Threading.Tasks;
using CommunicationClientCommon;
using System.Diagnostics;


namespace CommunicationClientSL.Services
{
    public class AudioCommunicationService : CommonCommunicationService
    {
        MemoryAudioSink sink = new MemoryAudioSink();


        public void StartSendingAudio()
        {

            //if (captureSource.State != CaptureState.Stopped) captureSource.Stop();
            //captureSource.AudioCaptureDevice = CaptureDeviceConfiguration.GetDefaultAudioCaptureDevice();




            //if ((CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess()))
            //{
            sink = new MemoryAudioSink();

            AudioFormat desiredAudioFormat = null;
            foreach (AudioFormat audioFormat in captureSource.AudioCaptureDevice.SupportedFormats)
            {
                if (audioFormat.SamplesPerSecond == 8000 && audioFormat.BitsPerSample == 16 && audioFormat.Channels == 1 && audioFormat.WaveFormat == WaveFormatType.Pcm)
                {
                    desiredAudioFormat = audioFormat;
                }
            }

            if (desiredAudioFormat == null)
            {
                return;
            }

            captureSource.AudioCaptureDevice.DesiredFormat = desiredAudioFormat;

            sink.CaptureSource = captureSource;
            sink.OnBufferFulfill += sink_OnBufferFulfill_Async;
            captureSource.Start();
            sink.StartSending = true;


            //    }




        }


        async void sink_OnBufferFulfill_Async(object sender, EventArgs e)
        {
            //if (!AudioStarted)
            //    CallVideoTimer();
            await Task.Factory.StartNew(() =>
            {
                MessageBox.Show("Hi");
                byte[] PCM_Buffer = (byte[])sender;
                AudioStarted = true;
               
                byte[] Encoded = VoiceEncoder.G711Audio.ALawEncoder.ALawEncode(PCM_Buffer);
                SendData(CommunicationFlag.LiveAudio, Encoded);

            });


        }

    }
}