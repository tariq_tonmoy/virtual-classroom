﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Diagnostics;
using CommunicationClientCommon;



namespace CommunicationClientSL.Services
{
    public class CommonCommunicationService
    {
        protected static CaptureSource captureSource = new CaptureSource();
        public static bool IsCaptureSourceAvailavle { get; set; }



        protected static bool AudioStarted { get; set; }

        //bool ReceiveContinue = true;
        //byte[] receivingBuffer;

        public delegate void DataReadyToSendDelegate(CommunicationFlag commFlag, byte[] buffer);
        public event DataReadyToSendDelegate OnDataReadyToSend;


        protected void SendData(CommunicationFlag commFlag, byte[] buffer)
        {
            if (OnDataReadyToSend != null)
            {
                OnDataReadyToSend(commFlag, buffer);

            }

        }

        public static void SetCaptureSource()
        {
            if (captureSource != null)
            {

                if (captureSource.State != CaptureState.Stopped) captureSource.Stop();
                captureSource.VideoCaptureDevice = CaptureDeviceConfiguration.GetDefaultVideoCaptureDevice();
                captureSource.AudioCaptureDevice = CaptureDeviceConfiguration.GetDefaultAudioCaptureDevice();



                if ((CaptureDeviceConfiguration.AllowedDeviceAccess || CaptureDeviceConfiguration.RequestDeviceAccess()))
                {
                    IsCaptureSourceAvailavle = true;
                }

                

            }
        }


        protected delegate void Timer_Tick_Delegate(object sender, EventArgs e, VideoCommunicationService VideoService);
        protected static event Timer_Tick_Delegate On_Video_Timer_Tick_Required;
        //public  static VideoCommunicationService VideoService { get; set; }
        //public static Sender SenderInstance { get; set; }

        //public static AudioCommunicationService AudioService { get; set; }
        //protected void CallVideoTimer()
        //{
        //    if (On_Video_Timer_Tick_Required != null)
        //    {
        //        On_Video_Timer_Tick_Required(null, null, VideoService);
        //    }

        //}

    }
}
