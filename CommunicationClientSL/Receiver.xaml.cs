﻿using CommunicationClientCommon;
using CommunicationClientCommon.ViewModels;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VoiceEncoder;

namespace CommunicationClientSL
{
    public partial class Receiver : UserControl
    {
        HubConnection connection = null;
        IHubProxy proxy = null;

        PcmToWave pcm = new PcmToWave();
        Queue<byte[]> VideoBuffers = new Queue<byte[]>(), AudioBuffers = new Queue<byte[]>();
        bool AudioReceived = false, VideoReceived = false;

        public Receiver()
        {
            InitializeComponent();
            string serverUri = new Uri(HtmlPage.Document.DocumentUri, "/").ToString();
            connection = new HubConnection(serverUri, true);
            proxy = connection.CreateHubProxy("CommunicationHub");
            this.DisconnectButton.IsEnabled = false;
            ReceiveData();

        }

        async void ReceiveData()
        {
            await Task.Factory.StartNew(() =>
            {
                proxy.On<OnlineUsersViewModel, CommunicationFlag, byte[]>("ReceiveData", (messageSource, commFlag, buffer) =>
                {
                    if (commFlag == CommunicationClientCommon.CommunicationFlag.LiveText)
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            this.ShowMessageTextBlock.Text += messageSource.Username + ": " + Encoding.UTF8.GetString(buffer, 0, buffer.Length) + "\n";

                        });
                    else if (commFlag == CommunicationFlag.LiveVideo)
                    {

                        VideoReceived = true;
                        VideoBuffers.Enqueue(buffer);
                        if (true || AudioReceived)
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                MemoryStream ms = new MemoryStream(VideoBuffers.Dequeue());
                                BitmapImage bi = new BitmapImage();
                                bi.SetSource(ms);

                                ReceivedImage.Source = bi;

                                ms.Close();
                            });


                        }
                    }
                    else if (commFlag == CommunicationFlag.LiveAudio)
                    {
                        AudioReceived = true;
                        AudioBuffers.Enqueue(buffer);
                        if (true || VideoReceived)
                        {
                            byte[] tempBuffer = AudioBuffers.Dequeue();
                            byte[] decodedBuffer = new byte[tempBuffer.Length * 2];
                            VoiceEncoder.G711Audio.ALawDecoder.ALawDecode(tempBuffer, out decodedBuffer); ;


                            MemoryStream ms_PCM = new MemoryStream(decodedBuffer);
                            MemoryStream ms_Wave = new MemoryStream();

                            pcm.SavePcmToWav(ms_PCM, ms_Wave, 16, 8000, 1);

                            WaveMediaStreamSource waveStream = new WaveMediaStreamSource(ms_Wave);
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                VoiceMedia.SetSource(waveStream);
                                VoiceMedia.Play();
                            });
                        }
                    }

                });
            });
        }

        private async void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (connection.State != ConnectionState.Connected)
                {
                    await connection.Start();

                }
                this.ConnectButton.IsEnabled = false;
                this.DisconnectButton.IsEnabled = true;

                await proxy.Invoke("RegisterAsReceiver", new OnlineUsersViewModel() { Username = (SenderText.Text == "" ? "sender" : SenderText.Text) }, new OnlineUsersViewModel() { Username = (UsernameText.Text == "" ? "receiver" : UsernameText.Text) });
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private async void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (connection.State == ConnectionState.Connected)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        connection.Stop();
                    });

                    this.ConnectButton.IsEnabled = true;
                    this.DisconnectButton.IsEnabled = false;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private async void MessageTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string msg = this.MessageTextBox.Text;
                byte[] b = Encoding.UTF8.GetBytes(msg);
                await proxy.Invoke("CallbackFromReceiver", new OnlineUsersViewModel() { Username = (UsernameText.Text == "" ? "receiver" : UsernameText.Text) }, CommunicationFlag.LiveText, b);
                this.ShowMessageTextBlock.Text += UsernameText.Text + ": " + MessageTextBox.Text + "\n";
                MessageTextBox.Text = "";
            }
        }
    }
}
