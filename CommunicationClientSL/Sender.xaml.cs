﻿using CommunicationClientCommon;
using CommunicationClientCommon.ViewModels;
using CommunicationClientSL.Services;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using VoiceEncoder;
namespace CommunicationClientSL
{
    public partial class Sender : UserControl
    {

        HubConnection connection = null;
        IHubProxy proxy = null;
        static Rectangle videoRectangle;
        VideoCommunicationService videoService;
        AudioCommunicationService audioService;
        public Sender()
        {
            InitializeComponent();

            string serverUri = new Uri(HtmlPage.Document.DocumentUri, "/").ToString();
            connection = new HubConnection(serverUri, true);
            proxy = connection.CreateHubProxy("CommunicationHub");
            this.disconnectButton.IsEnabled = false;
            proxy.On<OnlineUsersViewModel, CommunicationFlag, byte[]>("ReceiveData", (messageSource, commFlag, buffer) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    if (commFlag == CommunicationFlag.LiveText)
                        this.msgFromHub.Text += messageSource.Username + ": " + Encoding.UTF8.GetString(buffer, 0, buffer.Length) + "\n";
                });
            });


            videoService = new VideoCommunicationService();
            videoService.OnVideoSourceSet += videoService_OnVideoSourceSet;
            videoService.OnDataReadyToSend += Communication_OnDataReadyToSend;

            audioService = new AudioCommunicationService();
            audioService.OnDataReadyToSend += Communication_OnDataReadyToSend;
          

            //CommonCommunicationService.VideoService = videoService;
            //CommonCommunicationService.AudioService = audioService;
            //CommonCommunicationService.SenderInstance = this;



        }

        private async void sendMsgButton_Click(object sender, RoutedEventArgs e)
        {
            string msg = this.msgTextBox.Text;
            byte[] b = Encoding.UTF8.GetBytes(msg);
            await proxy.Invoke("SendToReceivers", new OnlineUsersViewModel() { Username = (nameTextBox.Text == "" ? "sender" : nameTextBox.Text) }, CommunicationFlag.LiveText, b);

        }

        private async void connectButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                if (connection.State != ConnectionState.Connected)
                {
                    await connection.Start();

                }
                this.connectButton.IsEnabled = false;
                this.disconnectButton.IsEnabled = true;

                await proxy.Invoke("RegisterAsSender", new OnlineUsersViewModel() { Username = (nameTextBox.Text == "" ? "sender" : nameTextBox.Text) });
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private async void disconnectButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                if (connection.State == ConnectionState.Connected)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        connection.Stop();
                    });

                    this.connectButton.IsEnabled = true;
                    this.disconnectButton.IsEnabled = false;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void StopVideo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void StartVideo_Click(object sender, RoutedEventArgs e)
        {
            CommonCommunicationService.SetCaptureSource();
            if (CommonCommunicationService.IsCaptureSourceAvailavle)
            {
                //Thread th = new Thread(new ThreadStart(videoService.StartSendingVideo));

                //th = new Thread(new ThreadStart(audioService.StartSendingAudio));
                videoService.StartSendingVideo();
                audioService.StartSendingAudio();
            }
            

        }
        PcmToWave pcm = new PcmToWave();


        async void Communication_OnDataReadyToSend(CommunicationFlag commFlag, byte[] buffer)
        {
            await proxy.Invoke("SendToReceivers", new OnlineUsersViewModel() { Username = (nameTextBox.Text == "" ? "sender" : nameTextBox.Text) }, commFlag, buffer);
        }

        void videoService_OnVideoSourceSet(VideoBrush videoBrush)
        {
            VideoRect.Fill = videoBrush;
            Sender.videoRectangle = VideoRect;
        }

        public static Rectangle GetVideoRect()
        {
            return Sender.videoRectangle;
        }


    }
}
